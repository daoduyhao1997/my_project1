//
//  UIViewHao.swift
//  demo
//
//  Created by DuyHao on 9/8/18.
//  Copyright © 2018 DuyHao. All rights reserved.
//

import UIKit

class UIViewHao: UIView {
    @IBOutlet weak var txtSDT: UITextField!
  
    
    @IBAction func txtNEXT(_ sender: Any) {
        
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        let view = UINib(nibName: String(describing: type(of: self)), bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = frame
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
