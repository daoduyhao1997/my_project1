//
//  ViewControllerMain.swift
//  demo
//
//  Created by DuyHao on 9/8/18.
//  Copyright © 2018 DuyHao. All rights reserved.
//

import UIKit

class ViewControllerMain: UIViewController {
    @IBOutlet weak var _layoutHao: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewHao = UIViewHao.init(frame: UIScreen.main.bounds)
        _layoutHao.addSubview(viewHao)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}
